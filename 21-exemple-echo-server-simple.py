# /usr/bin/python
#-*- coding: utf-8-8*-
#
# exemple-echo-server.py
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Febrer 2023
# -------------------------------------

import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print ("Conn", type(conn), conn)
print("Connected by", addr)
while True:
    data=conn.recv(1024)
    if not data:
        break
    conn.send(b'data')
conn.close()
sys.exit(0)