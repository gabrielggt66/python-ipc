# /usr/bin/python
#-*- coding: utf-8-8*-
#
# signal.py
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Gener 2023
# -------------------------------------
import sys, os, signal,argparse

parser=argparse.ArgumentParser(description="gestionar alarma")
parser.add_argument("segons",type=int,help="segons")
args=parser.parse_args()
print(args)

#-------------------------------------
up=0
down = 0
def myusr1(signum,frame):
    global up
    print("Signal handler with signal:", signum)
    print("S'han afegit 60 segons al temporizador ")
    actual=signal.alarm(0)
    signal.alarm(actual+60)
    up += 1
def myusr2(signum,frame):
    global down
    actual=signal.alarm(0)
    if actual < 60:
        print("Signal handler with signal:", signum)
        print("Error no es pot descomptar perque el temperitzador te un valor inferior a 60")
        signal.alarm(actual)
    else:
        print("Signal handler with signal:", signum)
        print("S'han descomptat 60 segons al temporizador")
        actual=signal.alarm(0)
        signal.alarm(actual-60)
        down += 1
    
def mostrar(signum,frame):
    falten=signal.alarm(0)
    signal.alarm(falten)
    print("Signal handler with signal:", signum)
    print("El numero de segons restants es de", falten )

def myalarm(signum,frame):
    print("Finalitzant ... up",up, "down:",down)

def myhup(signum,frame):
    print("Signal handler with signal:", signum)
    print("restoring value: ",args.segons)
    signal.alarm(args.segons)

signal.signal(signal.SIGINT,signal.SIG_IGN) # 2
signal.signal(signal.SIGHUP,myhup) # 1
signal.signal(signal.SIGUSR1,myusr1) # 10
signal.signal(signal.SIGUSR2,myusr2) # 12
signal.signal(signal.SIGALRM,myalarm) # 14
signal.signal(signal.SIGTERM,mostrar) # 15


signal.alarm(args.segons)
while True:
    pass
signal.alarm(0)
sys.exit(0)