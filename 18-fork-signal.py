# /usr/bin/python
#-*- coding: utf-8-8*-
#
# exemple-fork.py
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Febrer 2023
# -------------------------------------


import sys,os,signal
print("Hola, començant amb el programa principal")
print("PID pare: ", os.getpid())


def myusr1(signum,frame):
    print("Signal handler with signal:", signum)
    print("Hola radiola")

def myusr2(signum,frame):
    print("Signal handler with signal:", signum)
    print("Adeu andreu")
    sys.exit(0)



pid=os.fork()
if pid !=0:
    #os.wait()
    print("Programa pare", os.getpid(),pid)
    print("Hasta luego lucas !")
    sys.exit(0)

#Codi del programa fill
print("Programa fill", os.getpid(),pid)
signal.signal(signal.SIGUSR1,myusr1) # 10
signal.signal(signal.SIGUSR2,myusr2) # 12
while True:
    pass


