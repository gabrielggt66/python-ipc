# /usr/bin/python
#-*- coding: utf-8-8*-
#
# daytime-server-one2one.py [-p port]
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Febrer 2023
# -------------------------------------

import sys,socket,argparse
from subprocess import  PIPE

parser = argparse.ArgumentParser(description=\
        """Client daytime """)
parser.add_argument("-s",type=str,\
        help="Direccio IP server",metavar="server", dest='server',default='')        
parser.add_argument("-p",type=int,\
        help="Port",metavar="port",dest='port',default=50001)
args=parser.parse_args()
#--------------------------------------
import sys,socket
HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:
    data=s.recv(1024)
    if not data:
        break
    print('data', repr(data))
s.close()
sys.exit(0)