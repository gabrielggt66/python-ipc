# /usr/bin/python
#-*- coding: utf-8-8*-
#
# ps-client-one2one.py  [-p port] server
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Febrer 2023
# -------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """Client ps ax """)
parser.add_argument("server",type=str,\
        help="Direccio IP server")        
parser.add_argument("-p",type=int,\
        help="Port",dest='port',default=50001)
args=parser.parse_args()
#---------------------------------------
HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
command = "ps ax "
pipeData = Popen(command,shell=True,stdout=PIPE)
for line in pipeData.stdout:
    s.send(line)
s.close() 
sys.exit(0)