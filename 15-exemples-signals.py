# /usr/bin/python
#-*- coding: utf-8-8*-
#
# signal-exemple.py
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Gener 2023
# -------------------------------------
import sys, os, signal
def myhandler(signum, frame):
    print("Signal handler with signal:", signum)
    print("hasta luego lucas!")
    sys.exit(1)
def mydeath(signum, frame):
    print("Signal handler with signal:", signum)
    print("no em dona la gana morir")

#Assignar un handler al senyal
signal.signal(signal.SIGALRM,myhandler) # 14
signal.signal(signal.SIGUSR2,myhandler) # 12
signal.signal(signal.SIGUSR1,mydeath) # 10
signal.signal(signal.SIGINT,signal.SIG_IGN) # 10

signal.alarm(60)
print(os.getpid)
while True:
    pass
signal.alarm(0)
sys.exit(0)
