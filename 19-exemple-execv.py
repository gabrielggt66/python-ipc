# /usr/bin/python
#-*- coding: utf-8-8*-
#
# exemple-execv.py
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Febrer 2023
# -------------------------------------

import sys,os
print("Hola, començant amb el programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    #os.wait()
    print("Programa pare", os.getpid(),pid)
    sys.exit(0)

print("Programa fill", os.getpid(),pid)
#os.execv("/usr/bin/ls",["/usr/bin/ls","-la","/"]) #Lista o tupla pero se puede asignar estas en un avariable
#os.execl("/usr/bin/ls","/usr/bin/ls","-lh", "/opt") #Obligado a hacerlo Argumento por argumento
#os.execlp("ls","ls","-lh","/opt") #No hace falta poner el path absoulto de la comanda porqeu el ya lo busca en el PATH(/usr/bin)
#os.execvp("uname",["uname","-a"]) #una combinacion de execv i execp
#os.execv("/bin/bash",["/bin/bash","show.sh"])
os.execle("bin/bash","/bin/bash","show.sh",{"nom":"juan","edat":"25"})#Se define el env con un dicionario
print("Hasta luego lucas !")
sys.exit(0)