# /usr/bin/python
#-*- coding: utf-8-8*-
#
# popen.py ruta
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Gener 2023
# psql -qtA -F','  -h 172.17.0.2 -U postgres -d training -c "select * from oficinas;"
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description=\
        """Exemple popen """)
parser.add_argument("ruta",type=str,\
        help="directori a llistar")
args=parser.parse_args()
# -------------------------------------
command = [ "psql -qtA -F',' -h localhost -U postgres training -c \"select * from oficinas;\""]
pipeData= Popen(command,shell=True, stdout=PIPE)
for line in pipeData.stdout:
    print(line.decode("utf8"),end="")
exit(0)