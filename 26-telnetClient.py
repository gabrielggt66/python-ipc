# /usr/bin/python
#-*- coding: utf-8-8*-
#
# daytime-server-one2one.py [-p port]
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Febrer 2023
# -------------------------------------

import sys,socket,argparse
from subprocess import  PIPE

parser = argparse.ArgumentParser(description=\
        """Client daytime """)
parser.add_argument("-s",type=str,\
        help="Direccio IP server",metavar="server", dest='server',default='')        
parser.add_argument("-p",type=int,\
        help="Port",metavar="port",dest='port',default=50001)
args=parser.parse_args()
#--------------------------------------
HOST = args.server
PORT = args.port
MYEOF = bytes(chr(4), 'utf-8')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))
while True:
    ordre=input()
    if not ordre: 
        break 
    print(bytes(ordre, 'utf-8'))
    s.sendall(bytes(ordre, 'utf-8'))
    while True:
        data=s.recv(1024)
        if  data[-1:] == MYEOF:
            print( str(data[:-1]))
            break
        print(str(data))

                

s.close()
sys.exit(0)