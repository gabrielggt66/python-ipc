# /usr/bin/python
#-*- coding: utf-8-8*-
#
# popen-sql.py -d database -c numclie[...]
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Gener 2023
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description=\
        """Exemple popen-injectat """)
parser.add_argument("-d","--database",type=str,\
        help="Base de dades",metavar="database",dest="database",default="training")
parser.add_argument("-c","--client",type=int,\
        help="Numero de client",metavar="num_clie",dest="num_clie",required="True",action="append")
args=parser.parse_args()
print(args)

# -------------------------------------
cmd = [f"psql -qtA -F',' -h localhost -U postgres {args.database}"]
pipeData = Popen(cmd, shell = True, bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
#pipeData.stdin.write("select * from oficinas;\n\q\n")
for clie in args.num_clie:
        pipeData.stdin.write(f"select * from clientes where num_clie={clie};" + "\n")
        print(pipeData.stdout.readline(),end="")
pipeData.stdin.write("\q\n")
exit(0)