# /usr/bin/python
#-*- coding: utf-8-8*-
#
# daytime-server-one2one.py [-p port]
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Febrer 2023
# -------------------------------------

import sys,socket,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """Exemple popen-inje """)
parser.add_argument("-p",type=int,\
        help="Port",metavar="port",dest='port',default=50001)
args=parser.parse_args()
#----------------------------------------
HOST = ''
PORT = args.port 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
while True:
    conn, addr = s.accept()
    print ("Conn", type(conn), conn)
    print("Connected by", addr)

    cmd=[f'date']
    pipeData = Popen(cmd, stdout=PIPE)
    for line in pipeData.stdout:
        conn.send(line)
    conn.close()
