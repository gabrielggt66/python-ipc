# /usr/bin/python
#-*- coding: utf-8-8*-
#
# popen-sql.py consulta-sql
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Gener 2023
# psql -qtA -F','  -h 172.17.0.2 -U postgres -d training -c "select * from oficinas;"
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description=\
        """Exemple popen-injectat """)
parser.add_argument("sqlstatement",type=str,\
        help="Sentencia psql",metavar="sentencia_sql")
args=parser.parse_args()

# -------------------------------------
cmd = [ "psql -qtA -F',' -h localhost -U postgres training  "]
pipeData= Popen(cmd,shell=True, stdout=PIPE,stdin=PIPE,stderr=PIPE,bufsize=0,universal_newlines=True)
#pipeData.stdin.write("select * from oficinas;\n\q\n")
pipeData.stdin.write(args.sqlstatement+"\n\q\n")
for line in pipeData.stdout:
    print(line,end="")
exit(0)