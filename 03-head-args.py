# /usr/bin/python
#-*- coding: utf-8-8*-
#
#head  [-n nlin] [-f file]
#   10 lines,file o stdin
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Gener 2023
# -----------------------------
import argparse

parser = argparse.ArgumentParser(\
    description="programa head que mostra les primeres li9 nies de un fitxer")

parser.add_argument("-n" , "--nlin",type=int,\
                    dest="nlin", help="numero de linies a procesar",metavar="numLinies", \
                    default=10)
parser.add_argument("-f" , "--fit" ,type=str,\
            dest="fitxer",metavar="fitxer", help="ficher a processar",default="/dev/stdin")
args=parser.parse_args()

#-------------------------------
MAXLIN=args.nlin
fileIn=open(args.fitxer,"r")
counter=0
for line in fileIn:
    counter+=1
    print(line,end="")
    if counter == MAXLIN: break
fileIn.close()
exit(0)