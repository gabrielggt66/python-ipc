# /usr/bin/python
#-*- coding: utf-8-8*-
#
# exemple-echo-server.py
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Febrer 2023
# -------------------------------------

import sys,socket
from subprocess import Popen, PIPE
HOST = ''
PORT = 50002
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print ("Conn", type(conn), conn)
print("Connected by", addr)

cmd=[f'date']
pipeData = Popen(cmd, stdout=PIPE)
for line in pipeData.stdout:
    conn.send(line)
conn.close()
sys.exit(0)