# /usr/bin/python
#-*- coding: utf-8-8*-
#
# exemple-execv.py
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Febrer 2023
# -------------------------------------

import sys,socket
HOST = ''
PORT = 50002
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:
    data=s.recv(1024)
    if not data:
        break
    print('data', repr(data))
s.close()
sys.exit(0)