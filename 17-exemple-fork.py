# /usr/bin/python
#-*- coding: utf-8-8*-
#
# exemple-fork.py
#------------------------------
# @gabrielggt66 ASIX M06 Curs 2022-2023
# Febrer 2023
# -------------------------------------

import sys,os
print("Hola, començant amb el programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    #os.wait()
    print("Programa pare", os.getpid(),pid)
else:
    print("Programa fill", os.getpid(),pid)
    while True:
        pass

print("Hasta luego lucas !")
sys.exit(0)